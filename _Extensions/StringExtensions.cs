﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KiesNet.RestClient.Core
{
    internal static class StringExtensions
    {
        /// <summary>
        /// Konvertiert einen string zu Base64
        /// </summary>
        /// <param name="value">String, welcher umgewandelt wird</param>
        /// <returns>Den in Base64 umgewandelten String</returns>
        public static string ToBase64(this string value)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
        }
    }
}
