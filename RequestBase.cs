﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KiesNet.RestClient.Core
{
    public class RequestBase
    {
        public static string DefaultRootUrl { get; set; } = string.Empty;
        public static IAuthentificationCredentials DefaultCredentials { get; set; }
        public static AuthentificationType DefaultAuthentificationType { get; set; }
        public static string DefaultContentType { get; set; } = "*/*";
        public static string DefaultAcceptHeader { get; set; } = "*/*";

        public event Action<WebRequest> BeforeRequest;
        public event EventHandler<RequestEventArgs> RequestFinished;

        public string RootUrl { get; set; }
        public Method Method { get; set; }
        public string AcceptHeader { get; set; } = DefaultAcceptHeader;
        public string ContentType { get; set; } = DefaultContentType;
        public string Path { get; set; }
        public string RequestBody { get; set; }

        public GetParameterCollection GetParameters { get; set; } = new GetParameterCollection();
        public PostParameterCollection PostParameters { get; set; } = new PostParameterCollection();

        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        public AuthentificationType AuthentificationType { get; set; }
        public IAuthentificationCredentials Credentials { get; set; } = DefaultCredentials;

        public TimeSpan? Timeout { get; set; }

        private WebRequest m_request;
        private CancellationTokenSource m_timeoutTokenSource;
        private CancellationToken m_timeoutToken;

        public RequestBase(string path, AuthentificationType authentificationType, Method method)
        {
            this.Path = path;
            this.Method = method;
            this.AuthentificationType = authentificationType;

            this.RootUrl = DefaultRootUrl;
        }

        public RequestBase(string path, Method method) : this(path, DefaultAuthentificationType, method)
        {
        }

        public RequestBase()
        {
        }

        public async Task<RequestResult> SendRequestAsync(CancellationToken token)
        {
            if (this.Timeout != null)
            {
                this.m_timeoutTokenSource = new CancellationTokenSource();
                this.m_timeoutToken = this.m_timeoutTokenSource.Token;

                this.m_timeoutToken.Register(() => throw new TaskCanceledException("Timeout reached"));

                this.m_timeoutTokenSource.CancelAfter(this.Timeout.Value);
            }

            token.Register(() => throw new TaskCanceledException("Request cancelled"));
            
            RequestResult result = null;

            try
            {
                HttpWebResponse response = await this.GetResponseAsync();

                if (response == null)
                    result = new RequestResult(string.Empty, HttpStatusCode.RequestTimeout);
                else
                    result = new RequestResult(await new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEndAsync(),
                                                     response.StatusCode);

                this.RequestFinished?.Invoke(this, new RequestEventArgs(result));

                return result;
            }
            catch (WebException exception)
            {
                result = new RequestResult(string.Empty, ((HttpWebResponse) exception.Response).StatusCode);
                return result;
            }
            finally
            {
                this.m_timeoutTokenSource?.Dispose();
                this.m_timeoutToken = CancellationToken.None;
            }
        }

        public async Task<RequestResult> SendRequestAsync()
        {
            return await this.SendRequestAsync(CancellationToken.None);
        }

        protected virtual Task<RequestToken> GetTokenAsync()
        {
            throw new NotImplementedException("Please override method");
        }

        private async Task<HttpWebResponse> GetResponseAsync()
        {
            string requestUrl = this.RootUrl + this.Path;

            if (this.GetParameters?.Any() ?? false)
            {
                requestUrl += $"?{this.GetParameters.ToParameterString()}";
            }

            this.m_request = WebRequest.Create(requestUrl);
            this.m_request.Proxy = null;
            this.m_request.Method = this.Method.ToString().ToUpper();
            
            await this.AddHeadersAsync();

            if (this.Method == Method.Post || this.Method == Method.Put)
            {
                await this.ConfigureRequestForFormAsync();
            }
            
            this.BeforeRequest?.Invoke(this.m_request);

            return await this.m_request.GetResponseAsync() as HttpWebResponse;
        }

        private async Task AddHeadersAsync()
        {
            // Add accept header
            this.m_request.Headers.Add("Accept", this.AcceptHeader.ToLower());

            // Set content type
            this.m_request.ContentType = this.ContentType.ToLower();

            switch (this.AuthentificationType)
            {
                case AuthentificationType.Base64:
                    this.m_request.Headers.Add("Authorization", "Basic " + $"{this.Credentials.Username}:{this.Credentials.Password}".ToBase64());
                    break;

                case AuthentificationType.Token:
                    RequestToken token = await this.GetTokenAsync();
                    this.m_request.Headers.Add("Authorization", $"{token.TokenType} {token.AccessToken}");
                    break;
            }

            foreach (KeyValuePair<string, string> header in this.Headers ?? new Dictionary<string, string>())
            {
                if (!this.m_request.Headers.AllKeys.Contains(header.Key))
                {
                    this.m_request.Headers.Add(header.Key, header.Value);
                }   
            }
        }

        private async Task ConfigureRequestForFormAsync()
        {
            using (StreamWriter requestWriter = new StreamWriter(await this.m_request.GetRequestStreamAsync()))
                switch (this.ContentType)
                {
                    case "application/x-www-form-urlencoded":
                        await requestWriter.WriteAsync(this.PostParameters.ToParameterString());
                        break;

                    default:
                        await requestWriter.WriteAsync(this.RequestBody);
                        break;
                }
        }
    }
}
