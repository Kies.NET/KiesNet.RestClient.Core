﻿using System;
using Newtonsoft.Json;

namespace KiesNet.RestClient.Core
{
    public class RequestToken
    {
        /// <summary>
        /// Tokentyp
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; }

        /// <summary>
        /// Token, welches dem zu authentifizierenden Request
        /// beigefügt wird
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; }

        /// <summary>
        /// Zeitspanne in Sekunden, nachdem das Token 
        /// seine Gültigkeit verliert
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; }

        /// <summary>
        /// Zeitpunkt, an dem das Token erstellt wurde
        /// </summary>
        public DateTime CreatedAt { get; }

        /// <summary>
        /// Zeitpunkt, an dem das Token ausläuft
        /// </summary>
        public DateTime ExpiresAt => this.CreatedAt.AddSeconds(this.ExpiresIn);

        public RequestToken(string tokenType, string accessToken, int expiresIn)
        {
            this.TokenType = tokenType;
            this.AccessToken = accessToken;
            this.ExpiresIn = expiresIn;

            this.CreatedAt = DateTime.Now;
        }

    }
}
