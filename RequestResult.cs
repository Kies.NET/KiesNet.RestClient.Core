﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace KiesNet.RestClient.Core
{
    public class RequestResult
    {
        public string Result { get; }
        public HttpStatusCode StatusCode { get; }

        public bool IsInformational => (int) this.StatusCode >= 100 && (int) this.StatusCode < 200;
        public bool IsSuccess => (int) this.StatusCode >= 200 && (int) this.StatusCode < 300;
        public bool IsRedirection => (int) this.StatusCode >= 300 && (int) this.StatusCode < 400;
        public bool IsClientError => (int) this.StatusCode >= 400 && (int) this.StatusCode < 500;
        public bool IsServerError => (int) this.StatusCode >= 500 && (int) this.StatusCode < 600;

        public RequestResult(string result, HttpStatusCode statusCode)
        {
            this.Result = result;
            this.StatusCode = statusCode;
        }
    }
}
