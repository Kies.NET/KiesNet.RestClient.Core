﻿using System;

namespace KiesNet.RestClient.Core
{
    public class RequestEventArgs : EventArgs
    {
        public RequestResult Result { get; }

        public RequestEventArgs(RequestResult result)
        {
            this.Result = result;
        }
    }
}
