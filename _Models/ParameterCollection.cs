﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KiesNet.RestClient.Core
{
    public class ParameterCollection : IEnumerable<KeyValuePair<string, string>>
    {
        private Dictionary<string, string> parameters = new Dictionary<string, string>();

        public string this[string key]
        {
            get => this.parameters.ContainsKey(key) ? this.parameters[key] : null;
            set
            {
                if (this.parameters.ContainsKey(key))
                {
                    this.parameters[key] = value;
                }
                else
                {
                    this.parameters.Add(key, value);
                }
            }
        }

        public void Remove(string key)
        {
            if (this.parameters.ContainsKey(key))
            {
                this.parameters.Remove(key);
            }
        }

        public string ToParameterString()
        {
            if (!this.Any())
            {
                return string.Empty;
            }

            StringBuilder parameterStringBuilder = new StringBuilder();

            for (int i = 0; i < this.parameters.Count; i++)
            {
                KeyValuePair<string, string> parameter = this.parameters.ElementAt(i);
                parameterStringBuilder.Append($"{parameter.Key}={parameter.Value ?? "null"}");

                if (i != this.parameters.Count - 1)
                {
                    parameterStringBuilder.Append("&");
                }
            }

            return parameterStringBuilder.ToString();
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return this.parameters.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.parameters.GetEnumerator();
        }
    }
}
