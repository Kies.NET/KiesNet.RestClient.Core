﻿using System.Security;

namespace KiesNet.RestClient.Core
{
    public interface IAuthentificationCredentials
    {
        string Username { get; set; }
        SecureString Password { get; set; }
    }
}
