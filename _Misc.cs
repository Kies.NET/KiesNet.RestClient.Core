﻿
namespace KiesNet.RestClient.Core
{
    public enum Method
    {
        Post,
        Get,
        Put,
        Patch,
        Delete
    }

    public enum AuthentificationType
    {
        None,
        Base64,
        Token
    }
}
